#include <stdio.h>
#include <stdlib.h>
#include <math.h>


/**
 * Struktura przechowywująca macierz trójdiagonalną.
 */
typedef struct {
    double *l;
    double *d;
    double *u;
    int n;
} TriMatrix;


/**
 * Funkcja wypisująca wektor na standardowe wyjście.
 */
void printVector(double *vector, int n) {
    int i;
    for(i = 0; i < n; i++) {
        printf("%10.6lf\n", vector[i]);
    }
}


/**
 * Funkcja zwracająca logartym dziesiętny
 */
double log10(double x) {
    return log(x) / log(10.0);
}


/**
 * Funkcja rozwiązująca układ równań z macierzą trójdiagonalną.
 * Rozwiązanie jest nadpisywane na wektorze.
 */
void tridiagonalSolve(TriMatrix *matrix, double *vector) {
    int i ;
    for(i = 1; i < matrix->n; i++) {
        matrix->d[i] -= matrix->l[i-1] * matrix->u[i-1] / matrix->d[i-1];
    }

    for(i = 1; i < matrix->n; i++) {
        vector[i] -= matrix->l[i-1] * vector[i-1] / matrix->d[i-1];
    }

    vector[matrix->n-1] /= matrix->d[matrix->n-1];
    for(i = matrix->n-2; i >= 0; i--) {
        vector[i] = (vector[i] - matrix->u[i] * vector[i+1]) / matrix->d[i];
    }
}


/**
 * Funkcja zwracająca wartości funkcji z rozwiązania analitycznego.
 */
double exact(double x) {
    return (exp(x / 2.0) - exp(2.0 - x / 2.0)) / (1.0 - exp(2.0));
}



/**
 * Wyznacza błąd maksymalny w rozwiązaniu rówania różniczkowego
 * metodą trzypunktowej dyskretyzacji konwencjonalnej.
 */
double conventional(int edges, FILE *file) {
    int i;

    // Alokacja pamięci na macierz i wektor
    TriMatrix *matrix = malloc(sizeof(TriMatrix));
    matrix->l = malloc((edges - 3) * sizeof(double));
    matrix->d = malloc((edges - 2) * sizeof(double));
    matrix->u = malloc((edges - 3) * sizeof(double));
    matrix->n = edges - 2;

    double *vector = malloc((edges - 2) * sizeof(double));

    // Długość kroku sieci jednorodnej
    double h = 2.0 / (edges - 1);

    // Wypełnienie macierzy i wektora danymi wejściowymi
    for(i = 0; i < edges - 3; i++) {
        matrix->l[i] = 1.0 / (h * h);
        matrix->d[i] = (-2.0 - 0.25 * h * h) / (h * h);
        matrix->u[i] = 1.0 / (h * h);
        vector[i+1] = 0.0;
    }
    matrix->d[edges-3] = (-2.0 - 0.25 * h * h) / (h * h);
    vector[0] = -1.0 / (h * h);

    tridiagonalSolve(matrix, vector);
    
    // Ustalnie błędu maksymalnego
    double maxError = 0.0;
    for(i = 0; i < edges - 2; i++) {
        double error = fabs(vector[i] - exact((i + 1) * h));
        if(error > maxError) {
            maxError = error;
        }
    }

    if(file != NULL) {
        fprintf(file, "%20.16lf %20.16lf\n", h, maxError);
    }

    // Zwolnienie zaalokowanej pamięci
    free(matrix->l);
    free(matrix->d);
    free(matrix->u);
    free(matrix);

    return maxError;
}


/**
 * Wyznacza błąd maksymalny w rozwiązaniu rówania różniczkowego
 * metodą dyskretyzacji Numerova.
 */
double numerov(int edges, FILE *file) {
    int i;

    // Alokacja pamięci na macierz i wektor
    TriMatrix *matrix = malloc(sizeof(TriMatrix));
    matrix->l = malloc((edges - 3) * sizeof(double));
    matrix->d = malloc((edges - 2) * sizeof(double));
    matrix->u = malloc((edges - 3) * sizeof(double));
    matrix->n = edges - 2;

    double *vector = malloc((edges - 2) * sizeof(double));

    // Długość kroku sieci jednorodnej
    double h = 2.0 / (edges - 1);

    // Wypełnienie macierzy i wektora danymi wejściowymi
    for(i = 0; i < edges - 3; i++) {
        matrix->l[i] = (48.0 - h * h) / 48.0;
        matrix->d[i] = (-48.0 - 5.0 * h * h) / 24.0;
        matrix->u[i] = (48.0 - h * h) / 48.0;
        vector[i+1] = 0.0;
    }
    matrix->d[edges-3] = (-48.0 - 5.0 * h * h) / 24.0;
    vector[0] = -(48.0 - h * h) / 48.0;

    tridiagonalSolve(matrix, vector);

    // Ustalnie błędu maksymalnego
    double maxError = 0.0;
    for(i = 0; i < edges - 2; i++) {
        double error = fabs(vector[i] - exact((i + 1) * h));
        if(error > maxError) {
            maxError = error;
        }
    }

    if(file != NULL) {
        fprintf(file, "%20.16lf %20.16lf\n", h, maxError);
    }

    // Zwolnienie zaalokowanej pamięci
    free(matrix->l);
    free(matrix->d);
    free(matrix->u);
    free(matrix);

    return maxError;
}

int main() {
    int i;
    double order;
    FILE *file;

    // Dyskretyzacja konwencjonalna
    file = fopen("conventional.dat", "w");
    for(i = 10; i < 30000; i+=50) {
        conventional(i, file);
    }
    fclose(file);
    order = 
        (log10(conventional(10, NULL)) - log10(conventional(100, NULL)))
        / (log10(2.0 / (10 - 1)) - log10(2.0 / (100 - 1)));
    printf("Rząd dokładności: %lf\n", order);

    // Dyskretyzacja Numerova
    file = fopen("numerov.dat", "w");
    for(i = 10; i < 2000; i+=5) {
        numerov(i, file);
    }
    fclose(file);
    order = 
        (log10(numerov(10, NULL)) - log10(numerov(100, NULL)))
        / (log10(2.0 / (10 - 1)) - log10(2.0 / (100 - 1)));
    printf("Rząd dokładności: %lf\n", order);

    return 0;
}
